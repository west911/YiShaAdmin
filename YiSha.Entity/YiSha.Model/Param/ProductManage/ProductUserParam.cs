﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:05
    /// 描 述：实体查询类
    /// </summary>
    public class ProductUserListParam
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string UserName { get; set; }
    }
}
