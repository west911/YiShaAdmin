﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:10
    /// 描 述：实体查询类
    /// </summary>
    public class ProductListParam
    {
        public string ProductName { get; set; }
    }
}
