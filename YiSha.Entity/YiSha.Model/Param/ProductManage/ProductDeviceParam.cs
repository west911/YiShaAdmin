﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:07
    /// 描 述：实体查询类
    /// </summary>
    public class ProductDeviceListParam
    {
        public long? ProductId { get; set; }
        public string ProductName { get; set; }

        public string DeviceName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
