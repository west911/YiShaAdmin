﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-09 20:09
    /// 描 述：实体查询类
    /// </summary>
    public class DaysListParam
    {
        public DateTime Date { get; set; }

        public DateTime EndDate { get; set; }
    }
}
