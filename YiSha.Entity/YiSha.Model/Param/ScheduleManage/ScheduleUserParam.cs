﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 22:59
    /// 描 述：实体查询类
    /// </summary>
    public class ScheduleUserListParam
    {
        public DateTime? Date { get; set; }
        public DateTime? EndDate { get; set; }
        public string ProductId { get; set; }
        public string UserId { get; set; }
    }
}
