﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-03 17:08
    /// 描 述：实体查询类
    /// </summary>
    public class ScheduleDeviceListParam
    {
        public DateTime? Date { get; set; }
        public DateTime? EndDate { get; set; }
        public string ProductId { get; set; }
        public string DeviceNo { get; set; }
    }
}
