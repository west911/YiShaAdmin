﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:33
    /// 描 述：实体查询类
    /// </summary>
    public class ExperimentFlowListParam
    {
        public long? ExperimentDataId { get; set; }
    }
}
