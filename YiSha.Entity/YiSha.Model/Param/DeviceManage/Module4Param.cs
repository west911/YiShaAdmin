﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-05-05 12:22
    /// 描 述：实体查询类
    /// </summary>
    public class Module4ListParam
    {
        public string Field60 { get; set; }
    }
}
