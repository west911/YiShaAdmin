﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using YiSha.Util;

namespace YiSha.Model.Param.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:23
    /// 描 述：实体查询类
    /// </summary>
    public class DeviceListParam
    {
        public string DeviceName { get; set; }
    }
}
