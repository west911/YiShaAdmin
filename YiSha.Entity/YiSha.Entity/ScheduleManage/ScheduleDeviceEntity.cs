﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-03 17:08
    /// 描 述：实体类
    /// </summary>
    [Table("system_schedule_device")]
    public class ScheduleDeviceEntity : BaseExtensionEntity
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public string CDay { get; set; }
        public DateTime Date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string DeviceName { get; set; }
        public string DeviceNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductId { get; set; }
        [NotMapped]
        public string ProductName { get; set; }
        public int? Count { get; set; }

        public int? AccountId { get; set; }

        public string AccountName { get; set; }

        public string AccountAddr { get; set; }
    }
}
