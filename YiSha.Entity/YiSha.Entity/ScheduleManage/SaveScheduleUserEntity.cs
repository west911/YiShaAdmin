﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-03 17:08
    /// 描 述：实体类
    /// </summary>
    public class SaveScheduleUserEntity : BaseExtensionEntity
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductId { get; set; }
        public int? Count { get; set; }
        public int? AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountAddr { get; set; }
    }
}
