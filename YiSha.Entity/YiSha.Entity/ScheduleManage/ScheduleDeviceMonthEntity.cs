﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ScheduleManage
{
    public class ScheduleDeviceMonthEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string DeviceName { get; set; }
        public string DeviceNo { get; set; }
        public int? DeviceCount { get; set; }
        public int? 一 { get; set; }
        public int? 二 { get; set; }
        public int? 三 { get; set; }
        public int? 四 { get; set; }
        public int? 五 { get; set; }
        public int? 六 { get; set; }
        public int? 七 { get; set; }
        public int? 八 { get; set; }
        public int? 九 { get; set; }
        public int? 十 { get; set; }
        public int? 十一 { get; set; }
        public int? 十二 { get; set; }
        public int? 十三 { get; set; }
        public int? 十四 { get; set; }
        public int? 十五 { get; set; }
        public int? 十六 { get; set; }
        public int? 十七 { get; set; }
        public int? 十八 { get; set; }
        public int? 十九 { get; set; }
        public int? 二十 { get; set; }
        public int? 二十一 { get; set; }
        public int? 二十二 { get; set; }
        public int? 二十三 { get; set; }
        public int? 二十四 { get; set; }
        public int? 二十五 { get; set; }
        public int? 二十六 { get; set; }
        public int? 二十七 { get; set; }
        public int? 二十八 { get; set; }
        public int? 二十九 { get; set; }
        public int? 三十 { get; set; }
        public int? 三十一 { get; set; }
    }
}
