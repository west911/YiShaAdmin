﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:07
    /// 描 述：实体类
    /// </summary>
    [Table("system_product_device")]
    public class ProductDeviceEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string DeviceName { get; set; }
        [NotMapped]
        public string DeviceNO { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string ProductName { get; set; }
        [NotMapped]
        public string Feature { get; set; }
        [NotMapped]
        public string BookNO { get; set; }
        [NotMapped]
        public string XingHao { get; set; }
        [NotMapped]
        public string FanWei { get; set; }
        [NotMapped]
        public string JiGou { get; set; }
        [NotMapped]
        public string XiShu { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? IsMustDevice { get; set; }
    }
}
