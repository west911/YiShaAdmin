﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:05
    /// 描 述：实体类
    /// </summary>
    [Table("system_product_user")]
    public class ProductUserEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ProductId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string ProductName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(StringJsonConverter))]
        public long? UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NotMapped]
        public string UserName { get; set; }
    }
}
