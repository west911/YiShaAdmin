﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:10
    /// 描 述：实体类
    /// </summary>
    [Table("system_product")]
    public class ProductEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ProductName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
    }
}
