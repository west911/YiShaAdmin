﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:23
    /// 描 述：实体类
    /// </summary>
    [Table("crm_module_1")]
    public class DeviceEntity : BaseEntity
    {
        /// <summary>
        /// ?豸????
        /// </summary>
        /// <returns></returns>
        public string Field1 { get; set; }
        /// <summary>
        /// Դݨɋ
        /// </summary>
        /// <returns></returns>
        public string CreateMan { get; set; }
        /// <summary>
        /// ԴݨɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateManText { get; set; }
        /// <summary>
        /// Դݨʱݤ
        /// </summary>
        /// <returns></returns>
        public int? CreateTime { get; set; }
        /// <summary>
        /// خ۳ўلɋ
        /// </summary>
        /// <returns></returns>
        public string UpdateMan { get; set; }
        /// <summary>
        /// خ۳ўلɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string UpdateManText { get; set; }
        /// <summary>
        /// خ۳ўلʱݤ
        /// </summary>
        /// <returns></returns>
        public int? UpdateTime { get; set; }
        /// <summary>
        /// ̹Ԑ֟
        /// </summary>
        /// <returns></returns>
        public string Owner { get; set; }
        /// <summary>
        /// ̹˴ҿą
        /// </summary>
        /// <returns></returns>
        public string OwnerDept { get; set; }
        /// <summary>
        /// Դݨҿą
        /// </summary>
        /// <returns></returns>
        public string CreateDept { get; set; }
        /// <summary>
        /// ԴݨҿąĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateDeptText { get; set; }
        /// <summary>
        /// ɾԽҪ݇
        /// </summary>
        /// <returns></returns>
        public int? Deleted { get; set; }
        /// <summary>
        /// ٲЭɋԱ
        /// </summary>
        /// <returns></returns>
        public string ShareMan { get; set; }
        /// <summary>
        /// ٲЭɋԱ֯طȨО
        /// </summary>
        /// <returns></returns>
        public string ShareOp { get; set; }
        /// <summary>
        /// ??????
        /// </summary>
        /// <returns></returns>
        public string Field2 { get; set; }
        /// <summary>
        /// ??????????
        /// </summary>
        /// <returns></returns>
        public string Field2Text { get; set; }
        /// <summary>
        /// ?ͺ?
        /// </summary>
        /// <returns></returns>
        public string Field3 { get; set; }
        /// <summary>
        /// ???к?
        /// </summary>
        /// <returns></returns>
        public string Field4 { get; set; }
        /// <summary>
        /// ״̬
        /// </summary>
        /// <returns></returns>
        public string Field5 { get; set; }
        /// <summary>
        /// λ??
        /// </summary>
        /// <returns></returns>
        public string Field6 { get; set; }
        /// <summary>
        /// ????
        /// </summary>
        /// <returns></returns>
        public string Field7 { get; set; }
        /// <summary>
        /// ʹ?÷?Χ
        /// </summary>
        /// <returns></returns>
        public string Field8 { get; set; }
        /// <summary>
        /// ????????
        /// </summary>
        /// <returns></returns>
        public string Field9 { get; set; }
        /// <summary>
        /// ????????????
        /// </summary>
        /// <returns></returns>
        public string Field9Text { get; set; }
        /// <summary>
        /// ??;
        /// </summary>
        /// <returns></returns>
        public string Field10 { get; set; }
        /// <summary>
        /// У׼ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field12 { get; set; }
        /// <summary>
        /// У׼??
        /// </summary>
        /// <returns></returns>
        public string Field13 { get; set; }
        /// <summary>
        /// У׼??????
        /// </summary>
        /// <returns></returns>
        public string Field13Text { get; set; }
        /// <summary>
        /// ?´?У׼ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field14 { get; set; }
        /// <summary>
        /// У׼??¼
        /// </summary>
        /// <returns></returns>
        public string Field15 { get; set; }
        /// <summary>
        /// У׼??¼????
        /// </summary>
        /// <returns></returns>
        public string Field15Text { get; set; }
        /// <summary>
        /// ?ڼ??˲?ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field16 { get; set; }
        /// <summary>
        /// ?´??ڼ??˲?ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field17 { get; set; }
        /// <summary>
        /// ?ڼ??˲?ִ????
        /// </summary>
        /// <returns></returns>
        public string Field18 { get; set; }
        /// <summary>
        /// ?ڼ??˲?ִ????????
        /// </summary>
        /// <returns></returns>
        public string Field18Text { get; set; }
        /// <summary>
        /// ?ڼ??˲???¼
        /// </summary>
        /// <returns></returns>
        public string Field19 { get; set; }
        /// <summary>
        /// ?ڼ??˲???¼????
        /// </summary>
        /// <returns></returns>
        public string Field19Text { get; set; }
        /// <summary>
        /// ά??ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field20 { get; set; }
        /// <summary>
        /// ?´?ά??ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field21 { get; set; }
        /// <summary>
        /// ά????
        /// </summary>
        /// <returns></returns>
        public string Field22 { get; set; }
        /// <summary>
        /// ά????????
        /// </summary>
        /// <returns></returns>
        public string Field22Text { get; set; }
        /// <summary>
        /// ά????¼
        /// </summary>
        /// <returns></returns>
        public string Field23 { get; set; }
        /// <summary>
        /// ά????¼????
        /// </summary>
        /// <returns></returns>
        public string Field23Text { get; set; }
        /// <summary>
        /// ????ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field24 { get; set; }
        /// <summary>
        /// ?´ε???ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field25 { get; set; }
        /// <summary>
        /// ??????
        /// </summary>
        /// <returns></returns>
        public string Field26 { get; set; }
        /// <summary>
        /// ??????????
        /// </summary>
        /// <returns></returns>
        public string Field26Text { get; set; }
        /// <summary>
        /// ??????¼
        /// </summary>
        /// <returns></returns>
        public string Field27 { get; set; }
        /// <summary>
        /// ??????¼????
        /// </summary>
        /// <returns></returns>
        public string Field27Text { get; set; }
        /// <summary>
        /// ?ɹ?????
        /// </summary>
        /// <returns></returns>
        public int? Field28 { get; set; }
        /// <summary>
        /// ??Ӧ??
        /// </summary>
        /// <returns></returns>
        public string Field29 { get; set; }
        /// <summary>
        /// ??Ӧ??????
        /// </summary>
        /// <returns></returns>
        public string Field29Text { get; set; }
        /// <summary>
        /// ?ʲ?????
        /// </summary>
        /// <returns></returns>
        public string Field30 { get; set; }
        /// <summary>
        /// У׼??ȷ????
        /// </summary>
        /// <returns></returns>
        public string Field31 { get; set; }
        /// <summary>
        /// ׼ȷ?ȵȼ?
        /// </summary>
        /// <returns></returns>
        public string Field32 { get; set; }
        /// <summary>
        /// ?豸ԭֵ????Ԫ??
        /// </summary>
        /// <returns></returns>
        public decimal? Field33 { get; set; }
        /// <summary>
        /// CMA?????豸????
        /// </summary>
        /// <returns></returns>
        public string Field35 { get; set; }
        /// <summary>
        /// CMA??????Ŀ
        /// </summary>
        /// <returns></returns>
        public string Field36 { get; set; }
        /// <summary>
        /// ????CMA????
        /// </summary>
        /// <returns></returns>
        public int? Field37 { get; set; }
        /// <summary>
        /// ўֽ٫ʽ
        /// </summary>
        /// <returns></returns>
        public string Field38 { get; set; }
        /// <summary>
        /// ??????ʽȷ??????
        /// </summary>
        /// <returns></returns>
        public string Field39 { get; set; }
        /// <summary>
        /// ??????ʽȷ??????????
        /// </summary>
        /// <returns></returns>
        public string Field39Text { get; set; }
        /// <summary>
        /// ά?޷?
        /// </summary>
        /// <returns></returns>
        public string Field40 { get; set; }
        /// <summary>
        /// ά?޷?????
        /// </summary>
        /// <returns></returns>
        public string Field40Text { get; set; }
        /// <summary>
        /// ά??ʱ??
        /// </summary>
        /// <returns></returns>
        public int? Field41 { get; set; }
        /// <summary>
        /// ά?޼?¼
        /// </summary>
        /// <returns></returns>
        public string Field42 { get; set; }
        /// <summary>
        /// ά?޼?¼????
        /// </summary>
        /// <returns></returns>
        public string Field42Text { get; set; }
        /// <summary>
        /// ֤????
        /// </summary>
        /// <returns></returns>
        public string Field43 { get; set; }
        /// <summary>
        /// ?Ƿ???Ҫ??3??У׼
        /// </summary>
        /// <returns></returns>
        public int? Field44 { get; set; }
        /// <summary>
        /// У׼֤???????????ձ?׼
        /// </summary>
        /// <returns></returns>
        public string Field45 { get; set; }
        /// <summary>
        /// ?Ƿ???Ҫ?????ڼ??˲?
        /// </summary>
        /// <returns></returns>
        public int? Field46 { get; set; }
        /// <summary>
        /// ?豸??????
        /// </summary>
        /// <returns></returns>
        public string Field47 { get; set; }
        /// <summary>
        /// ?豸??????????
        /// </summary>
        /// <returns></returns>
        public string Field47Text { get; set; }
        /// <summary>
        /// ?Ƿ???Ҫ????ά??????
        /// </summary>
        /// <returns></returns>
        public int? Field48 { get; set; }
        /// <summary>
        /// У׼??Χ
        /// </summary>
        /// <returns></returns>
        public string Field49 { get; set; }
        /// <summary>
        /// ??ע
        /// </summary>
        /// <returns></returns>
        public string Field50 { get; set; }
    }
}
