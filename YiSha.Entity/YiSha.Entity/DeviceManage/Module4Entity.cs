﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-05-05 12:22
    /// 描 述：实体类
    /// </summary>
    [Table("crm_module_4")]
    public class Module4Entity : BaseEntity
    {
        /// <summary>
        /// ???к?
        /// </summary>
        /// <returns></returns>
        public string Field1 { get; set; }
        /// <summary>
        /// Դݨҿą
        /// </summary>
        /// <returns></returns>
        public string CreateDept { get; set; }
        /// <summary>
        /// ԴݨҿąĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateDeptText { get; set; }
        /// <summary>
        /// Դݨɋ
        /// </summary>
        /// <returns></returns>
        public string CreateMan { get; set; }
        /// <summary>
        /// ԴݨɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateManText { get; set; }
        /// <summary>
        /// Դݨʱݤ
        /// </summary>
        /// <returns></returns>
        public int? CreateTime { get; set; }
        /// <summary>
        /// ɾԽҪ݇
        /// </summary>
        /// <returns></returns>
        public int? Deleted { get; set; }
        /// <summary>
        /// У׼/???ⵥλ
        /// </summary>
        /// <returns></returns>
        public string Field10 { get; set; }
        /// <summary>
        /// Ʒ??
        /// </summary>
        /// <returns></returns>
        public string Field2 { get; set; }
        /// <summary>
        /// Ʒ??????
        /// </summary>
        /// <returns></returns>
        public string Field2Text { get; set; }
        /// <summary>
        /// ٩Ӧʌ
        /// </summary>
        /// <returns></returns>
        public string Field29 { get; set; }
        /// <summary>
        /// ٩ӦʌĻԆ
        /// </summary>
        /// <returns></returns>
        public string Field29Text { get; set; }
        /// <summary>
        /// эۅ
        /// </summary>
        /// <returns></returns>
        public string Field3 { get; set; }
        /// <summary>
        /// Ʒ??(????)
        /// </summary>
        /// <returns></returns>
        public string Field4 { get; set; }
        /// <summary>
        /// ȷ?ϵ?λ
        /// </summary>
        /// <returns></returns>
        public string Field6 { get; set; }
        /// <summary>
        /// ȷ?Ϲ???ʦ
        /// </summary>
        /// <returns></returns>
        public string Field8 { get; set; }
        /// <summary>
        /// ̹Ԑ֟
        /// </summary>
        /// <returns></returns>
        public string Owner { get; set; }
        /// <summary>
        /// ̹˴ҿą
        /// </summary>
        /// <returns></returns>
        public string OwnerDept { get; set; }
        /// <summary>
        /// ٲЭɋԱ
        /// </summary>
        /// <returns></returns>
        public string ShareMan { get; set; }
        /// <summary>
        /// ٲЭɋԱ֯طȨО
        /// </summary>
        /// <returns></returns>
        public string ShareOp { get; set; }
        /// <summary>
        /// خ۳ўلɋ
        /// </summary>
        /// <returns></returns>
        public string UpdateMan { get; set; }
        /// <summary>
        /// خ۳ўلɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string UpdateManText { get; set; }
        /// <summary>
        /// خ۳ўلʱݤ
        /// </summary>
        /// <returns></returns>
        public int? UpdateTime { get; set; }
        /// <summary>
        /// ؊ӺҠۅ
        /// </summary>
        /// <returns></returns>
        public string Field30 { get; set; }
        /// <summary>
        /// У׼????
        /// </summary>
        /// <returns></returns>
        public string Field31 { get; set; }
        /// <summary>
        /// ʹ?÷?Χ
        /// </summary>
        /// <returns></returns>
        public string Field32 { get; set; }
        /// <summary>
        /// ݆`Ұ
        /// </summary>
        /// <returns></returns>
        public string Field33 { get; set; }
        /// <summary>
        /// اЕǀڀ`Ұ
        /// </summary>
        /// <returns></returns>
        public string Field34 { get; set; }
        /// <summary>
        /// ʹԃƵÊ
        /// </summary>
        /// <returns></returns>
        public string Field35 { get; set; }
        /// <summary>
        /// ژݼԌ׈
        /// </summary>
        /// <returns></returns>
        public string Field36 { get; set; }
        /// <summary>
        /// SOP
        /// </summary>
        /// <returns></returns>
        public string Field37 { get; set; }
        /// <summary>
        /// ҩ֤ɕǚ
        /// </summary>
        /// <returns></returns>
        public int? Field42 { get; set; }
        /// <summary>
        /// ҩ֤ԐЧǚ
        /// </summary>
        /// <returns></returns>
        public int? Field43 { get; set; }
        /// <summary>
        /// ˵????
        /// </summary>
        /// <returns></returns>
        public string Field44 { get; set; }
        /// <summary>
        /// ˵????????
        /// </summary>
        /// <returns></returns>
        public string Field44Text { get; set; }
        /// <summary>
        /// άۤɕǚ
        /// </summary>
        /// <returns></returns>
        public int? Field45 { get; set; }
        /// <summary>
        /// άۤԐЧǚ
        /// </summary>
        /// <returns></returns>
        public int? Field46 { get; set; }
        /// <summary>
        /// ????SOP
        /// </summary>
        /// <returns></returns>
        public string Field48 { get; set; }
        /// <summary>
        /// ????SOP????
        /// </summary>
        /// <returns></returns>
        public string Field48Text { get; set; }
        /// <summary>
        /// У׼/ݬӢɕǚ
        /// </summary>
        /// <returns></returns>
        public int? Field49 { get; set; }
        /// <summary>
        /// У׼/ݬӢԐЧǚ
        /// </summary>
        /// <returns></returns>
        public int? Field50 { get; set; }
        /// <summary>
        /// У׼/???ⱨ??11111
        /// </summary>
        /// <returns></returns>
        public string Field51 { get; set; }
        /// <summary>
        /// У׼/ά??SOP
        /// </summary>
        /// <returns></returns>
        public string Field52 { get; set; }
        /// <summary>
        /// У׼/ά??SOP????
        /// </summary>
        /// <returns></returns>
        public string Field52Text { get; set; }
        /// <summary>
        /// ȷ?ϱ???
        /// </summary>
        /// <returns></returns>
        public string Field54 { get; set; }
        /// <summary>
        /// ȷ?ϱ???????
        /// </summary>
        /// <returns></returns>
        public string Field54Text { get; set; }
        /// <summary>
        /// ̹˴
        /// </summary>
        /// <returns></returns>
        public string Field55 { get; set; }
        /// <summary>
        /// ̹˴ĻԆ
        /// </summary>
        /// <returns></returns>
        public string Field55Text { get; set; }
        /// <summary>
        /// ҿą
        /// </summary>
        /// <returns></returns>
        public string Field56 { get; set; }
        /// <summary>
        /// У׼/???ⱨ??
        /// </summary>
        /// <returns></returns>
        public string Field57 { get; set; }
        /// <summary>
        /// У׼/???ⱨ??????
        /// </summary>
        /// <returns></returns>
        public string Field57Text { get; set; }
        /// <summary>
        /// ?豸?ڲ???ת????
        /// </summary>
        /// <returns></returns>
        public string Field58 { get; set; }
        /// <summary>
        /// ?ͻ??豸?????ڲ?????
        /// </summary>
        /// <returns></returns>
        public string Field59 { get; set; }
        /// <summary>
        /// ?豸????
        /// </summary>
        /// <returns></returns>
        public string Field60 { get; set; }
        /// <summary>
        /// ?ͻ???ϵ??
        /// </summary>
        /// <returns></returns>
        public string Field61 { get; set; }
        /// <summary>
        /// ?ͻ???ϵ??????
        /// </summary>
        /// <returns></returns>
        public string Field61Text { get; set; }
        /// <summary>
        /// ?ͻ?????
        /// </summary>
        /// <returns></returns>
        public string Field62 { get; set; }
        /// <summary>
        /// ???۵???
        /// </summary>
        /// <returns></returns>
        public string Field63 { get; set; }
        /// <summary>
        /// ??װλ??????
        /// </summary>
        /// <returns></returns>
        public string Field64 { get; set; }
        /// <summary>
        /// ??;????
        /// </summary>
        /// <returns></returns>
        public string Field65 { get; set; }
        /// <summary>
        /// ??װλ??Ӣ??
        /// </summary>
        /// <returns></returns>
        public string Field66 { get; set; }
        /// <summary>
        /// ??;Ӣ??
        /// </summary>
        /// <returns></returns>
        public string Field67 { get; set; }
        /// <summary>
        /// ?豸????Ӣ??
        /// </summary>
        /// <returns></returns>
        public string Field68 { get; set; }
        /// <summary>
        /// ?豸????
        /// </summary>
        /// <returns></returns>
        public string Field69 { get; set; }
        /// <summary>
        /// ?û?????Ӣ??
        /// </summary>
        /// <returns></returns>
        public string Field70 { get; set; }
        /// <summary>
        /// ?ͻ???ϵ?绰
        /// </summary>
        /// <returns></returns>
        public string Field71 { get; set; }
    }
}
