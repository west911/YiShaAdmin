﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-09 20:09
    /// 描 述：实体类
    /// </summary>
    [Table("system_days")]
    public class DaysEntity : BaseEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? Month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? Day { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string CDay { get; set; }
        public DateTime Date { get; set; }
    }
}
