﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:33
    /// 描 述：实体类
    /// </summary>
    [Table("system_experiment_flow")]
    public class ExperimentFlowEntity : BaseExtensionEntity
    {
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ExperimentDataId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string OwnerIds { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int? Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
    }
}
