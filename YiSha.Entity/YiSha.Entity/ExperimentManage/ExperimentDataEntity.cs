﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:30
    /// 描 述：实体类
    /// </summary>
    [Table("system_experiment_data")]
    public class ExperimentDataEntity : BaseExtensionEntity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ExperimentType { get; set; }
        public string ExperimentNumber { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? SumbitDatetime { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long? OwnerIds { get; set; }
        public string JoinIds { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ReviewIds { get; set; }
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ReviewDatetime { get; set; }
        [JsonConverter(typeof(StringJsonConverter))]
        public long? ConfirmIds { get; set; }
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ConfirmDatetime { get; set; }
        public int? Status { get; set; }
        public string AccountName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Datas { get; set; }
        public string Step { get; set; }
        public string DeviceLicNo { get; set; }
        public string DeviceNo { get; set; }
    }
}
