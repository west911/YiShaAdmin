﻿using System;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using YiSha.Util;

namespace YiSha.Entity.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-13 08:57
    /// 描 述：实体类
    /// </summary>
    [Table("crm_account")]
    public class AccountEntity : BaseEntity
    {
        /// <summary>
        /// Դݨʱݤ
        /// </summary>
        /// <returns></returns>
        public int? CreateTime { get; set; }
        /// <summary>
        /// خ۳ўلʱݤ
        /// </summary>
        /// <returns></returns>
        public int? UpdateTime { get; set; }
        /// <summary>
        /// Դݨɋ
        /// </summary>
        /// <returns></returns>
        public string CreateMan { get; set; }
        /// <summary>
        /// خ۳ўلɋ
        /// </summary>
        /// <returns></returns>
        public string UpdateMan { get; set; }
        /// <summary>
        /// ̹Ԑ֟
        /// </summary>
        /// <returns></returns>
        public string Owner { get; set; }
        /// <summary>
        /// ̹˴ҿą
        /// </summary>
        /// <returns></returns>
        public string OwnerDept { get; set; }
        /// <summary>
        /// Դݨҿą
        /// </summary>
        /// <returns></returns>
        public string CreateDept { get; set; }
        /// <summary>
        /// ɾԽҪ݇
        /// </summary>
        /// <returns></returns>
        public int? Deleted { get; set; }
        /// <summary>
        /// ٲЭɋԱ
        /// </summary>
        /// <returns></returns>
        public string ShareMan { get; set; }
        /// <summary>
        /// ٲЭɋԱ֯طȨО
        /// </summary>
        /// <returns></returns>
        public string ShareOp { get; set; }
        /// <summary>
        /// ?ͻ?????
        /// </summary>
        /// <returns></returns>
        public string AccountName { get; set; }
        /// <summary>
        /// ԴݨɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateManText { get; set; }
        /// <summary>
        /// خ۳ўلɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string UpdateManText { get; set; }
        /// <summary>
        /// ԴݨҿąĻԆ
        /// </summary>
        /// <returns></returns>
        public string CreateDeptText { get; set; }
        /// <summary>
        /// ?ͻ?????
        /// </summary>
        /// <returns></returns>
        public string AccountType { get; set; }
        /// <summary>
        /// ࠍۧєҰ
        /// </summary>
        /// <returns></returns>
        public string AccountSex { get; set; }
        /// <summary>
        /// ࠍۧҠۅ
        /// </summary>
        /// <returns></returns>
        public string AccountCode { get; set; }
        /// <summary>
        /// ʏܶࠍۧ
        /// </summary>
        /// <returns></returns>
        public string AccountParent { get; set; }
        /// <summary>
        /// ʏܶࠍۧĻԆ
        /// </summary>
        /// <returns></returns>
        public string AccountParentText { get; set; }
        /// <summary>
        /// ࠍۧjϵɋ
        /// </summary>
        /// <returns></returns>
        public string AccountContact { get; set; }
        /// <summary>
        /// ֤ݾ`э
        /// </summary>
        /// <returns></returns>
        public string CertificateKind { get; set; }
        /// <summary>
        /// ֤ݾۅë
        /// </summary>
        /// <returns></returns>
        public string CertificateCode { get; set; }
        /// <summary>
        /// ҿą
        /// </summary>
        /// <returns></returns>
        public string AccountDept { get; set; }
        /// <summary>
        /// ҿąĻԆ
        /// </summary>
        /// <returns></returns>
        public string AccountDeptText { get; set; }
        /// <summary>
        /// ࠍۧژϵ
        /// </summary>
        /// <returns></returns>
        public string AccountRela { get; set; }
        /// <summary>
        /// ࠍۧֈܶ
        /// </summary>
        /// <returns></returns>
        public string AccountLevel { get; set; }
        /// <summary>
        /// ??ҵ
        /// </summary>
        /// <returns></returns>
        public string AccountIndustry { get; set; }
        /// <summary>
        /// ѐҵĨ˶
        /// </summary>
        /// <returns></returns>
        public string IndustryDesc { get; set; }
        /// <summary>
        /// ࠍۧ4Դ
        /// </summary>
        /// <returns></returns>
        public string AccountSource { get; set; }
        /// <summary>
        /// غհɋ
        /// </summary>
        /// <returns></returns>
        public string AccountPrincipal { get; set; }
        /// <summary>
        /// غհɋĻԆ
        /// </summary>
        /// <returns></returns>
        public string AccountPrincipalText { get; set; }
        /// <summary>
        /// ̹՚ʡ
        /// </summary>
        /// <returns></returns>
        public string AccountAddrProvince { get; set; }
        /// <summary>
        /// ̹՚ː
        /// </summary>
        /// <returns></returns>
        public string AccountAddrCity { get; set; }
        /// <summary>
        /// ̹՚И
        /// </summary>
        /// <returns></returns>
        public string AccountAddrCounty { get; set; }
        /// <summary>
        /// ׷Ҫַ֘
        /// </summary>
        /// <returns></returns>
        public string AccountAddr { get; set; }
        /// <summary>
        /// ֧ؓԊݾ
        /// </summary>
        /// <returns></returns>
        public string AccountEmail { get; set; }
        /// <summary>
        /// ֧۰
        /// </summary>
        /// <returns></returns>
        public string AccountPhone { get; set; }
        /// <summary>
        /// κκ
        /// </summary>
        /// <returns></returns>
        public string AccountWangwang { get; set; }
        /// <summary>
        /// MSN/QQ
        /// </summary>
        /// <returns></returns>
        public string AccountMsnQq { get; set; }
        /// <summary>
        /// θַ
        /// </summary>
        /// <returns></returns>
        public string AccountUrl { get; set; }
        /// <summary>
        /// ԫ֦
        /// </summary>
        /// <returns></returns>
        public string AccountFax { get; set; }
        /// <summary>
        /// ˖ܺۅë
        /// </summary>
        /// <returns></returns>
        public string AccountMobile { get; set; }
        /// <summary>
        /// ԊҠ
        /// </summary>
        /// <returns></returns>
        public string AccountPost { get; set; }
        /// <summary>
        /// ˕ܵںݒ
        /// </summary>
        /// <returns></returns>
        public string AccountDeliveryCountry { get; set; }
        /// <summary>
        /// ˕ܵԇː
        /// </summary>
        /// <returns></returns>
        public string AccountDeliveryCity { get; set; }
        /// <summary>
        /// ַ֘2
        /// </summary>
        /// <returns></returns>
        public string AccountDeliveryAddr { get; set; }
        /// <summary>
        /// ַ֘3
        /// </summary>
        /// <returns></returns>
        public string AccountPayAddr { get; set; }
        /// <summary>
        /// ߪۧѐ
        /// </summary>
        /// <returns></returns>
        public string AccountBank { get; set; }
        /// <summary>
        /// Ӹѐ֋ۅ
        /// </summary>
        /// <returns></returns>
        public string AccountBankAccount { get; set; }
        /// <summary>
        /// хԃ׮׈
        /// </summary>
        /// <returns></returns>
        public string AccountCredit { get; set; }
        /// <summary>
        /// ޭӪҵϱ
        /// </summary>
        /// <returns></returns>
        public string AccountBusiness { get; set; }
        /// <summary>
        /// Ԇν
        /// </summary>
        /// <returns></returns>
        public string AccountSalutation { get; set; }
        /// <summary>
        /// ְϱ
        /// </summary>
        /// <returns></returns>
        public string AccountPosition { get; set; }
        /// <summary>
        /// ࠍۧʺɕ
        /// </summary>
        /// <returns></returns>
        public int? AccountBirhday { get; set; }
        /// <summary>
        /// ࠍۧݛֵ
        /// </summary>
        /// <returns></returns>
        public string AccountWorth { get; set; }
        /// <summary>
        /// Ѹע
        /// </summary>
        /// <returns></returns>
        public string Remark { get; set; }
        /// <summary>
        /// ߪۧɋ
        /// </summary>
        /// <returns></returns>
        public string AccountBankPerson { get; set; }
        /// <summary>
        /// ࠍۧʺɕ
        /// </summary>
        /// <returns></returns>
        public int? AccountBirthday { get; set; }
        /// <summary>
        /// ࠍۧ״̬
        /// </summary>
        /// <returns></returns>
        public string AccountStatus { get; set; }
        /// <summary>
        /// ࠍۧޗ׎
        /// </summary>
        /// <returns></returns>
        public string AccountPhase { get; set; }
        /// <summary>
        /// Ա٤ڦģ
        /// </summary>
        /// <returns></returns>
        public string AccountScale { get; set; }
        /// <summary>
        /// ԋӵ·П
        /// </summary>
        /// <returns></returns>
        public string GettingThere { get; set; }
        /// <summary>
        /// Ɉ֣ࠍۧ
        /// </summary>
        /// <returns></returns>
        public string HotSpot { get; set; }
        /// <summary>
        /// خ۳һՎٺ޸ʱݤ
        /// </summary>
        /// <returns></returns>
        public int? LastFollowTime { get; set; }
        /// <summary>
        /// ٫˾є׊
        /// </summary>
        /// <returns></returns>
        public string CompanyNature { get; set; }
        /// <summary>
        /// ࠍۧ`э
        /// </summary>
        /// <returns></returns>
        public string AccountKind { get; set; }
        /// <summary>
        /// ض࠮׽ʽ
        /// </summary>
        /// <returns></returns>
        public string Field7 { get; set; }
    }
}
