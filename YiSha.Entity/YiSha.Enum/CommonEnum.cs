﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YiSha.Enum
{
    public enum StatusEnum
    {
        [Description("启用")]
        Yes = 1,

        [Description("禁用")]
        No = 0
    }

    public enum IsEnum
    {
        [Description("是")]
        Yes = 1,

        [Description("否")]
        No = 0
    }

    public enum NeedEnum
    {
        [Description("不需要")]
        NotNeed = 0,

        [Description("需要")]
        Need = 1
    }

    public enum OperateStatusEnum
    {
        [Description("失败")]
        Fail = 0,

        [Description("成功")]
        Success = 1
    }

    public enum UploadFileType
    {
        [Description("头像")]
        Portrait = 1,

        [Description("新闻图片")]
        News = 2,

        [Description("导入的文件")]
        Import = 10
    }

    public enum PlatformEnum
    {
        [Description("Web后台")]
        Web = 1,

        [Description("WebApi")]
        WebApi = 2
    }

    public enum PayStatusEnum
    {
        [Description("未知")]
        Unknown = 0,

        [Description("已支付")]
        Success = 1,

        [Description("转入退款")]
        Refund = 2,

        [Description("未支付")]
        NotPay = 3,

        [Description("已关闭")]
        Closed = 4,

        [Description("已撤销（付款码支付）")]
        Revoked = 5,

        [Description("用户支付中（付款码支付）")]
        UserPaying = 6,

        [Description("支付失败(其他原因，如银行返回失败)")]
        PayError = 7
    }

    public enum DaysEnum
    {
        一 = 1,
        二 = 2,
        三 = 3,
        四 = 4,
        五 = 5,
        六 = 6,
        七 = 7,
        八 = 8,
        九 = 9,
        十 = 10,
        十一 = 11,
        十二 = 12,
        十三 = 13,
        十四 = 14,
        十五 = 15,
        十六 = 16,
        十七 = 17,
        十八 = 18,
        十九 = 19,
        二十 = 20,
        二十一 = 21,
        二十二 = 22,
        二十三 = 23,
        二十四 = 24,
        二十五 = 25,
        二十六 = 26,
        二十七 = 27,
        二十八 = 28,
        二十九 = 29,
        三十 = 30,
        三十一 = 31,
    }

}
