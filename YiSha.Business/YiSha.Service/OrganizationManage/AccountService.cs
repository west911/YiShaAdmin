﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.OrganizationManage;
using YiSha.Model.Param.OrganizationManage;

namespace YiSha.Service.OrganizationManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-13 08:57
    /// 描 述：服务类
    /// </summary>
    public class AccountService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<AccountEntity>> GetList(AccountListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<AccountEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<AccountEntity>> GetPageList(AccountListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<AccountEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<AccountEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(AccountEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<AccountEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<AccountEntity, bool>> ListFilter(AccountListParam param)
        {
            var expression = LinqExtensions.True<AccountEntity>();
            if (param != null)
            {
            }
            return expression;
        }

        private List<DbParameter> ListFilter(AccountListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  id as Id,
                                    account_name as AccountName,
                                    account_type as AccountType,
                                    account_code as AccountCode,
                                    account_addr as AccountAddr
                            FROM    crm_account
                            WHERE   1 = 1 and deleted = 0");
            var parameter = new List<DbParameter>();
            if (param != null)
            {

            }
            return parameter;
        }
        #endregion
    }
}
