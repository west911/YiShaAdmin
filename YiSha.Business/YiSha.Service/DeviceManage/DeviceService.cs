﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.DeviceManage;
using YiSha.Model.Param.DeviceManage;

namespace YiSha.Service.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:23
    /// 描 述：服务类
    /// </summary>
    public class DeviceService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<DeviceEntity>> GetList(DeviceListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<DeviceEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<DeviceEntity>> GetPageList(DeviceListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<DeviceEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<DeviceEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(DeviceEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<DeviceEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<DeviceEntity, bool>> ListFilter(DeviceListParam param)
        {
            var expression = LinqExtensions.True<DeviceEntity>();
            if (param != null)
            {
                expression = expression.And(t => t.Deleted == 0);
                if (!string.IsNullOrEmpty(param.DeviceName))
                {
                    expression = expression.And(t => t.Field1.Contains(param.DeviceName));
                }
            }
            return expression;
        }

        private List<DbParameter> ListFilter(DeviceListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  id as Id,
                                    field1 as Field1,
                                    field30 as Field30,
                                    create_man as CreateMan,
                                    field5 as Field5
                            FROM    crm_module_1
                            WHERE   1 = 1 and deleted = 0 and field5 = '使用中' and field1 is not null and field1 != '' group by field1");
            var parameter = new List<DbParameter>();
            if (param != null)
            {

            }
            return parameter;
        }
        #endregion
    }
}
