﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.DeviceManage;
using YiSha.Model.Param.DeviceManage;

namespace YiSha.Service.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-05-05 12:22
    /// 描 述：服务类
    /// </summary>
    public class Module4Service :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<Module4Entity>> GetList(Module4ListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<Module4Entity>> GetPageList(Module4ListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<Module4Entity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<Module4Entity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(Module4Entity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<Module4Entity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<Module4Entity, bool>> ListFilter(Module4ListParam param)
        {
            var expression = LinqExtensions.True<Module4Entity>();
            if (param != null)
            {
                expression = expression.And(t => t.Deleted == 0);
                if (!string.IsNullOrEmpty(param.Field60))
                {
                    expression = expression.And(t => t.Field60.Contains(param.Field60));
                }
            }
            return expression;
        }
        #endregion
    }
}
