﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.ProductManage;
using YiSha.Model.Param.ProductManage;

namespace YiSha.Service.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:07
    /// 描 述：服务类
    /// </summary>
    public class ProductDeviceService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<ProductDeviceEntity>> GetList(ProductDeviceListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ProductDeviceEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<ProductDeviceEntity>> GetNOList(ProductDeviceListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListNOFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ProductDeviceEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<ProductDeviceEntity>> GetPageList(ProductDeviceListParam param, Pagination pagination)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ProductDeviceEntity>(strSql.ToString(), filter.ToArray(), pagination);
            return list.ToList();
        }

        public async Task<ProductDeviceEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<ProductDeviceEntity>(id);
        }

        public async Task<ProductDeviceEntity> SearchEntity(long productId, string deviceName)
        {
            return await this.BaseRepository().FindEntity<ProductDeviceEntity>(s=>s.DeviceName == deviceName && s.ProductId == productId);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(ProductDeviceEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<ProductDeviceEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private List<DbParameter> ListFilter(ProductDeviceListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  a.id as Id,
                                    a.base_is_delete as BaseIsDelete,
                                    a.base_create_time as BaseCreateTime,
                                    a.base_modify_time as BaseModifyTime,
                                    a.base_creator_id as BaseCreatorId,
                                    a.base_modifier_id as BaseModifierId,
                                    a.base_version as BaseVersion,
                                    a.product_id as ProductId,
                                    a.device_name as DeviceName,
                                    a.is_must_device as IsMustDevice,
                                    b.product_name as ProductName
                            FROM    system_product_device a
                                    LEFT JOIN system_product b ON a.product_id = b.id
                            WHERE   1 = 1 and b.product_name like '%" + param.ProductName + "%' " +
                            "and a.device_name like '%" + param.DeviceName + "%'");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                if (param.ProductId!=null) 
                {
                    strSql.Append(@" and b.id = " + param.ProductId + "");
                }
            }
            return parameter;
        }

        private List<DbParameter> ListNOFilter(ProductDeviceListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  a.id as Id,
                                    a.base_is_delete as BaseIsDelete,
                                    a.base_create_time as BaseCreateTime,
                                    a.base_modify_time as BaseModifyTime,
                                    a.base_creator_id as BaseCreatorId,
                                    a.base_modifier_id as BaseModifierId,
                                    a.base_version as BaseVersion,
                                    a.product_id as ProductId,
                                    a.device_name as DeviceName,
                                    a.is_must_device as IsMustDevice,
                                    b.product_name as ProductName,
                                    c.field30 as DeviceNO,
                                    c.field31 as Feature,
                                    c.field43 as BookNO,
                                    c.field3 as XingHao,
                                    c.field8 as FanWei,
                                    c.field13_text as JiGou,
                                    c.field38 as XiShu
                            FROM    system_product_device a
                                    inner JOIN system_product b ON a.product_id = b.id
                                    inner JOIN crm_module_1 c ON a.device_name = c.field1 and c.field5 = '使用中' and c.deleted = 0 
                            WHERE   1 = 1 ");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                if (param.ProductId != null)
                {
                    strSql.Append(@" and b.id = " + param.ProductId + "");
                }
                if (param.StartDate != null && param.EndDate != null)
                {
                    strSql.Append(@" and c.field30 not in (select device_no from `system_schedule_device` where date >= '" + param.StartDate + "' and date <= '" + param.EndDate + "') ");
                }
            }
            strSql.Append(@" order by a.device_name,c.id");
            return parameter;
        }
        #endregion
    }
}
