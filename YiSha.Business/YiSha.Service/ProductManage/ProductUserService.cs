﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.ProductManage;
using YiSha.Model.Param.ProductManage;

namespace YiSha.Service.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:05
    /// 描 述：服务类
    /// </summary>
    public class ProductUserService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<ProductUserEntity>> GetList(ProductUserListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ProductUserEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<ProductUserEntity>> GetPageList(ProductUserListParam param, Pagination pagination)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ProductUserEntity>(strSql.ToString(), filter.ToArray(), pagination);
            return list.ToList();
        }

        public async Task<ProductUserEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<ProductUserEntity>(id);
        }

        public async Task<ProductUserEntity> SearchEntity(long productId, long userId)
        {
            return await this.BaseRepository().FindEntity<ProductUserEntity>(s=>s.ProductId == productId && s.UserId == userId);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(ProductUserEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<ProductUserEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private List<DbParameter> ListFilter(ProductUserListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  a.id as Id,
                                    a.base_is_delete as BaseIsDelete,
                                    a.base_create_time as BaseCreateTime,
                                    a.base_modify_time as BaseModifyTime,
                                    a.base_creator_id as BaseCreatorId,
                                    a.base_modifier_id as BaseModifierId,
                                    a.base_version as BaseVersion,
                                    a.product_id as ProductId,
                                    b.product_name as ProductName,
                                    a.user_id as UserId,
                                    c.user_name as UserName
                            FROM    system_product_user a
                                    LEFT JOIN system_product b ON a.product_id = b.id
                                    LEFT JOIN sys_user c ON a.user_id = c.id
                            WHERE   1 = 1 and b.product_name like '%" + param.ProductName + "%' " +
                            "and c.user_name like '%" + param.UserName + "%' ");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                if (param.ProductId != null)
                {
                    strSql.Append(@" and b.id = " + param.ProductId + "");
                }
            }
            return parameter;
        }
        #endregion
    }
}
