﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.ScheduleManage;
using YiSha.Model.Param.ScheduleManage;

namespace YiSha.Service.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 22:59
    /// 描 述：服务类
    /// </summary>
    public class ScheduleUserService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<ScheduleUserEntity>> GetList(ScheduleUserListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ScheduleUserEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<ScheduleUserEntity>> GetPageList(ScheduleUserListParam param, Pagination pagination)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ScheduleUserEntity>(strSql.ToString(), filter.ToArray(), pagination);
            return list.ToList();
        }

        public async Task<List<ScheduleUserMonthEntity>> GetListByMonth(ScheduleUserListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListByMonthFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ScheduleUserMonthEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<List<ScheduleUserMonthEntity>> GetAllListByDate(ScheduleListParam param)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListAllByDateFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ScheduleUserMonthEntity>(strSql.ToString(), filter.ToArray());
            return list.ToList();
        }

        public async Task<ScheduleUserEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<ScheduleUserEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(ScheduleUserEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<ScheduleUserEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private List<DbParameter> ListFilter(ScheduleUserListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  a.id as Id,
                                    a.base_is_delete as BaseIsDelete,
                                    a.base_create_time as BaseCreateTime,
                                    a.base_modify_time as BaseModifyTime,
                                    a.base_creator_id as BaseCreatorId,
                                    e.user_name as BaseCreatorName,
                                    a.base_modifier_id as BaseModifierId,
                                    a.base_version as BaseVersion,
                                    a.year as Year,
                                    a.month as Month,
                                    a.day as Day,
                                    a.cday as CDay,
                                    a.date as Date,
                                    a.user_id as UserId,
                                    c.user_name as UserName,
                                    a.product_id as ProductId,
                                    b.product_name as ProductName,
                                    a.count as Count,
                                    a.account_id as AccountId,
                                    d.account_name as AccountName,
                                    a.account_addr as AccountAddr
                            FROM    system_schedule_user a
                                    inner JOIN system_product b ON a.product_id = b.id 
                                    inner join sys_user c on a.user_id = c.id 
                                    inner join crm_account as d on a.account_id = d.id
                                    inner join sys_user as e on e.id = a.base_creator_id
                            WHERE   1 = 1");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                if (param.Date != null) 
                {
                    strSql.Append(@" and a.date >= '" + param.Date + "'");
                }
                if (param.EndDate != null) 
                {
                    strSql.Append(@" and a.date <= '" + param.EndDate + "'");
                }
                if (param.UserId != null)
                {
                    strSql.Append(@" and a.user_id = " + param.UserId);
                }
            }
            return parameter;
        }

        private List<DbParameter> ListByMonthFilter(ScheduleUserListParam param, StringBuilder strSql)
        {
            strSql.Append(@"CALL sp_pivot_user(@SDate, @EDate, @ProductId);");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                parameter.Add(DbParameterExtension.CreateDbParameter("@SDate", param.Date));
                parameter.Add(DbParameterExtension.CreateDbParameter("@EDate", param.EndDate));
                parameter.Add(DbParameterExtension.CreateDbParameter("@ProductId", param.ProductId));
            }
            return parameter;
        }

        private List<DbParameter> ListAllByDateFilter(ScheduleListParam param, StringBuilder strSql)
        {
            strSql.Append(@"CALL sp_schedule_user(@SDate, @EDate, @ProductId);");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                parameter.Add(DbParameterExtension.CreateDbParameter("@SDate", param.Date));
                parameter.Add(DbParameterExtension.CreateDbParameter("@EDate", param.EndDate));
                parameter.Add(DbParameterExtension.CreateDbParameter("@ProductId", param.ProductId));
            }
            return parameter;
        }
        #endregion
    }
}
