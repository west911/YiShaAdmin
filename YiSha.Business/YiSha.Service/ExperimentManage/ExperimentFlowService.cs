﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.ExperimentManage;
using YiSha.Model.Param.ExperimentManage;

namespace YiSha.Service.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:33
    /// 描 述：服务类
    /// </summary>
    public class ExperimentFlowService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<ExperimentFlowEntity>> GetList(ExperimentFlowListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<ExperimentFlowEntity>> GetPageList(ExperimentFlowListParam param, Pagination pagination)
        {
            var strSql = new StringBuilder();
            List<DbParameter> filter = ListFilter(param, strSql);
            var list = await this.BaseRepository().FindList<ExperimentFlowEntity>(strSql.ToString(), filter.ToArray(), pagination);
            return list.ToList();
        }

        public async Task<ExperimentFlowEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<ExperimentFlowEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(ExperimentFlowEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                await entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                await entity.Modify();
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<ExperimentFlowEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<ExperimentFlowEntity, bool>> ListFilter(ExperimentFlowListParam param)
        {
            var expression = LinqExtensions.True<ExperimentFlowEntity>();
            if (param != null)
            {
                if (param.ExperimentDataId != null)
                {
                    expression = expression.And(t => t.ExperimentDataId == param.ExperimentDataId);
                }
            }
            return expression;
        }

        private List<DbParameter> ListFilter(ExperimentFlowListParam param, StringBuilder strSql)
        {
            strSql.Append(@"SELECT  a.id as Id,
                                    a.base_is_delete as BaseIsDelete,
                                    a.base_create_time as BaseCreateTime,
                                    a.base_modify_time as BaseModifyTime,
                                    a.base_creator_id as BaseCreatorId,
                                    b.user_name as BaseCreatorName,
                                    a.base_modifier_id as BaseModifierId,
                                    a.base_version as BaseVersion,
                                    a.experiment_data_id as ExperimentDataId,
                                    a.status as Status,
                                    a.remark as Remark
                            FROM    system_experiment_flow a
                                    LEFT JOIN sys_user b ON a.base_creator_id = b.id
                            WHERE   1 = 1 ");
            var parameter = new List<DbParameter>();
            if (param != null)
            {
                if (param.ExperimentDataId != null)
                {
                    strSql.Append(@" and a.experiment_data_id = " + param.ExperimentDataId + "");
                }
            }
            return parameter;
        }
        #endregion
    }
}
