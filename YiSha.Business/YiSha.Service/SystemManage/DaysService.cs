﻿using System;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Data;
using YiSha.Data.Repository;
using YiSha.Entity.SystemManage;
using YiSha.Model.Param.SystemManage;

namespace YiSha.Service.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-09 20:09
    /// 描 述：服务类
    /// </summary>
    public class DaysService :  RepositoryFactory
    {
        #region 获取数据
        public async Task<List<DaysEntity>> GetList(DaysListParam param)
        {
            var expression = ListFilter(param);
            var list = await this.BaseRepository().FindList(expression);
            return list.ToList();
        }

        public async Task<List<DaysEntity>> GetPageList(DaysListParam param, Pagination pagination)
        {
            var expression = ListFilter(param);
            var list= await this.BaseRepository().FindList(expression, pagination);
            return list.ToList();
        }

        public async Task<DaysEntity> GetEntity(long id)
        {
            return await this.BaseRepository().FindEntity<DaysEntity>(id);
        }
        #endregion

        #region 提交数据
        public async Task SaveForm(DaysEntity entity)
        {
            if (entity.Id.IsNullOrZero())
            {
                entity.Create();
                await this.BaseRepository().Insert(entity);
            }
            else
            {
                
                await this.BaseRepository().Update(entity);
            }
        }

        public async Task DeleteForm(string ids)
        {
            long[] idArr = TextHelper.SplitToArray<long>(ids, ',');
            await this.BaseRepository().Delete<DaysEntity>(idArr);
        }
        #endregion

        #region 私有方法
        private Expression<Func<DaysEntity, bool>> ListFilter(DaysListParam param)
        {
            var expression = LinqExtensions.True<DaysEntity>();
            if (param != null)
            {
                if (param.Date != null)
                {
                    expression = expression.And(t => t.Date >= param.Date);
                }
                if (param.EndDate != null)
                {
                    expression = expression.And(t => t.Date <= param.EndDate);
                }
            }
            return expression;
        }
        #endregion
    }
}
