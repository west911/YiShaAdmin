﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ProductManage;
using YiSha.Model.Param.ProductManage;
using YiSha.Service.ProductManage;

namespace YiSha.Business.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:10
    /// 描 述：业务类
    /// </summary>
    public class ProductBLL
    {
        private ProductService productService = new ProductService();

        #region 获取数据
        public async Task<TData<List<ProductEntity>>> GetList(ProductListParam param)
        {
            TData<List<ProductEntity>> obj = new TData<List<ProductEntity>>();
            obj.Result = await productService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ProductEntity>>> GetPageList(ProductListParam param, Pagination pagination)
        {
            TData<List<ProductEntity>> obj = new TData<List<ProductEntity>>();
            obj.Result = await productService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ProductEntity>> GetEntity(long id)
        {
            TData<ProductEntity> obj = new TData<ProductEntity>();
            obj.Result = await productService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ProductEntity entity)
        {
            TData<string> obj = new TData<string>();
            await productService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await productService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
