﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ProductManage;
using YiSha.Model.Param.ProductManage;
using YiSha.Service.ProductManage;

namespace YiSha.Business.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:05
    /// 描 述：业务类
    /// </summary>
    public class ProductUserBLL
    {
        private ProductUserService productUserService = new ProductUserService();

        #region 获取数据
        public async Task<TData<List<ProductUserEntity>>> GetList(ProductUserListParam param)
        {
            TData<List<ProductUserEntity>> obj = new TData<List<ProductUserEntity>>();
            obj.Result = await productUserService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ProductUserEntity>>> GetPageList(ProductUserListParam param, Pagination pagination)
        {
            TData<List<ProductUserEntity>> obj = new TData<List<ProductUserEntity>>();
            obj.Result = await productUserService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ProductUserEntity>> GetEntity(long id)
        {
            TData<ProductUserEntity> obj = new TData<ProductUserEntity>();
            obj.Result = await productUserService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }

        public async Task<TData<ProductUserEntity>> SearchEntity(long productId, long userId)
        {
            TData<ProductUserEntity> obj = new TData<ProductUserEntity>();
            obj.Result = await productUserService.SearchEntity(productId, userId);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ProductUserEntity entity)
        {
            TData<string> obj = new TData<string>();
            await productUserService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await productUserService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
