﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ProductManage;
using YiSha.Model.Param.ProductManage;
using YiSha.Service.ProductManage;

namespace YiSha.Business.ProductManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:07
    /// 描 述：业务类
    /// </summary>
    public class ProductDeviceBLL
    {
        private ProductDeviceService productDeviceService = new ProductDeviceService();

        #region 获取数据
        public async Task<TData<List<ProductDeviceEntity>>> GetList(ProductDeviceListParam param)
        {
            TData<List<ProductDeviceEntity>> obj = new TData<List<ProductDeviceEntity>>();
            obj.Result = await productDeviceService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ProductDeviceEntity>>> GetNOList(ProductDeviceListParam param)
        {
            TData<List<ProductDeviceEntity>> obj = new TData<List<ProductDeviceEntity>>();
            obj.Result = await productDeviceService.GetNOList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ProductDeviceEntity>>> GetPageList(ProductDeviceListParam param, Pagination pagination)
        {
            TData<List<ProductDeviceEntity>> obj = new TData<List<ProductDeviceEntity>>();
            obj.Result = await productDeviceService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ProductDeviceEntity>> GetEntity(long id)
        {
            TData<ProductDeviceEntity> obj = new TData<ProductDeviceEntity>();
            obj.Result = await productDeviceService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }

        public async Task<TData<ProductDeviceEntity>> SearchEntity(long productId, string deviceName)
        {
            TData<ProductDeviceEntity> obj = new TData<ProductDeviceEntity>();
            obj.Result = await productDeviceService.SearchEntity(productId, deviceName);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ProductDeviceEntity entity)
        {
            TData<string> obj = new TData<string>();
            await productDeviceService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await productDeviceService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
