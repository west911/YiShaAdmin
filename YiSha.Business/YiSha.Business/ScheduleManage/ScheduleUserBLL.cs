﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ScheduleManage;
using YiSha.Model.Param.ScheduleManage;
using YiSha.Service.ScheduleManage;

namespace YiSha.Business.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 22:59
    /// 描 述：业务类
    /// </summary>
    public class ScheduleUserBLL
    {
        private ScheduleUserService scheduleUserService = new ScheduleUserService();

        #region 获取数据
        public async Task<TData<List<ScheduleUserEntity>>> GetList(ScheduleUserListParam param)
        {
            TData<List<ScheduleUserEntity>> obj = new TData<List<ScheduleUserEntity>>();
            obj.Result = await scheduleUserService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleUserEntity>>> GetPageList(ScheduleUserListParam param, Pagination pagination)
        {
            TData<List<ScheduleUserEntity>> obj = new TData<List<ScheduleUserEntity>>();
            obj.Result = await scheduleUserService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleUserMonthEntity>>> GetListByMonth(ScheduleUserListParam param)
        {
            TData<List<ScheduleUserMonthEntity>> obj = new TData<List<ScheduleUserMonthEntity>>();
            obj.Result = await scheduleUserService.GetListByMonth(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleUserMonthEntity>>> GetAllListByDate(ScheduleListParam param)
        {
            TData<List<ScheduleUserMonthEntity>> obj = new TData<List<ScheduleUserMonthEntity>>();
            obj.Result = await scheduleUserService.GetAllListByDate(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ScheduleUserEntity>> GetEntity(long id)
        {
            TData<ScheduleUserEntity> obj = new TData<ScheduleUserEntity>();
            obj.Result = await scheduleUserService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ScheduleUserEntity entity)
        {
            TData<string> obj = new TData<string>();
            await scheduleUserService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await scheduleUserService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
