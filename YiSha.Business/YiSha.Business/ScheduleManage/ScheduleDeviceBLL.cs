﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ScheduleManage;
using YiSha.Model.Param.ScheduleManage;
using YiSha.Service.ScheduleManage;

namespace YiSha.Business.ScheduleManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-03 17:08
    /// 描 述：业务类
    /// </summary>
    public class ScheduleDeviceBLL
    {
        private ScheduleDeviceService scheduleDeviceService = new ScheduleDeviceService();

        #region 获取数据
        public async Task<TData<List<ScheduleDeviceEntity>>> GetList(ScheduleDeviceListParam param)
        {
            TData<List<ScheduleDeviceEntity>> obj = new TData<List<ScheduleDeviceEntity>>();
            obj.Result = await scheduleDeviceService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleDeviceEntity>>> GetPageList(ScheduleDeviceListParam param, Pagination pagination)
        {
            TData<List<ScheduleDeviceEntity>> obj = new TData<List<ScheduleDeviceEntity>>();
            obj.Result = await scheduleDeviceService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleDeviceMonthEntity>>> GetListByMonth(ScheduleDeviceListParam param)
        {
            TData<List<ScheduleDeviceMonthEntity>> obj = new TData<List<ScheduleDeviceMonthEntity>>();
            obj.Result = await scheduleDeviceService.GetListByMonth(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ScheduleDeviceMonthEntity>>> GetAllListByDate(ScheduleListParam param)
        {
            TData<List<ScheduleDeviceMonthEntity>> obj = new TData<List<ScheduleDeviceMonthEntity>>();
            obj.Result = await scheduleDeviceService.GetAllListByDate(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ScheduleDeviceEntity>> GetEntity(long id)
        {
            TData<ScheduleDeviceEntity> obj = new TData<ScheduleDeviceEntity>();
            obj.Result = await scheduleDeviceService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ScheduleDeviceEntity entity)
        {
            TData<string> obj = new TData<string>();
            await scheduleDeviceService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await scheduleDeviceService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
