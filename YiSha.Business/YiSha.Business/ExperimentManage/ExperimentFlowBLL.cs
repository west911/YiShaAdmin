﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ExperimentManage;
using YiSha.Model.Param.ExperimentManage;
using YiSha.Service.ExperimentManage;

namespace YiSha.Business.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:33
    /// 描 述：业务类
    /// </summary>
    public class ExperimentFlowBLL
    {
        private ExperimentFlowService experimentFlowService = new ExperimentFlowService();

        #region 获取数据
        public async Task<TData<List<ExperimentFlowEntity>>> GetList(ExperimentFlowListParam param)
        {
            TData<List<ExperimentFlowEntity>> obj = new TData<List<ExperimentFlowEntity>>();
            obj.Result = await experimentFlowService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ExperimentFlowEntity>>> GetPageList(ExperimentFlowListParam param, Pagination pagination)
        {
            TData<List<ExperimentFlowEntity>> obj = new TData<List<ExperimentFlowEntity>>();
            obj.Result = await experimentFlowService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ExperimentFlowEntity>> GetEntity(long id)
        {
            TData<ExperimentFlowEntity> obj = new TData<ExperimentFlowEntity>();
            obj.Result = await experimentFlowService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ExperimentFlowEntity entity)
        {
            TData<string> obj = new TData<string>();
            await experimentFlowService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await experimentFlowService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
