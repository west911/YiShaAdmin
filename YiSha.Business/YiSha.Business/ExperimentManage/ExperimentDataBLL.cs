﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.ExperimentManage;
using YiSha.Model.Param.ExperimentManage;
using YiSha.Service.ExperimentManage;

namespace YiSha.Business.ExperimentManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:30
    /// 描 述：业务类
    /// </summary>
    public class ExperimentDataBLL
    {
        private ExperimentDataService experimentDataService = new ExperimentDataService();

        #region 获取数据
        public async Task<TData<List<ExperimentDataEntity>>> GetList(ExperimentDataListParam param)
        {
            TData<List<ExperimentDataEntity>> obj = new TData<List<ExperimentDataEntity>>();
            obj.Result = await experimentDataService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<ExperimentDataEntity>>> GetPageList(ExperimentDataListParam param, Pagination pagination)
        {
            TData<List<ExperimentDataEntity>> obj = new TData<List<ExperimentDataEntity>>();
            obj.Result = await experimentDataService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<ExperimentDataEntity>> GetEntity(long id)
        {
            TData<ExperimentDataEntity> obj = new TData<ExperimentDataEntity>();
            obj.Result = await experimentDataService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(ExperimentDataEntity entity)
        {
            TData<string> obj = new TData<string>();
            await experimentDataService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await experimentDataService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
