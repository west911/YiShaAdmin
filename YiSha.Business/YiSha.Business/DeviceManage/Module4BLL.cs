﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.DeviceManage;
using YiSha.Model.Param.DeviceManage;
using YiSha.Service.DeviceManage;

namespace YiSha.Business.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-05-05 12:22
    /// 描 述：业务类
    /// </summary>
    public class Module4BLL
    {
        private Module4Service module4Service = new Module4Service();

        #region 获取数据
        public async Task<TData<List<Module4Entity>>> GetList(Module4ListParam param)
        {
            TData<List<Module4Entity>> obj = new TData<List<Module4Entity>>();
            obj.Result = await module4Service.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<Module4Entity>>> GetPageList(Module4ListParam param, Pagination pagination)
        {
            TData<List<Module4Entity>> obj = new TData<List<Module4Entity>>();
            obj.Result = await module4Service.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<Module4Entity>> GetEntity(long id)
        {
            TData<Module4Entity> obj = new TData<Module4Entity>();
            obj.Result = await module4Service.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(Module4Entity entity)
        {
            TData<string> obj = new TData<string>();
            await module4Service.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await module4Service.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
