﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.DeviceManage;
using YiSha.Model.Param.DeviceManage;
using YiSha.Service.DeviceManage;

namespace YiSha.Business.DeviceManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:23
    /// 描 述：业务类
    /// </summary>
    public class DeviceBLL
    {
        private DeviceService deviceService = new DeviceService();

        #region 获取数据
        public async Task<TData<List<DeviceEntity>>> GetList(DeviceListParam param)
        {
            TData<List<DeviceEntity>> obj = new TData<List<DeviceEntity>>();
            obj.Result = await deviceService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<DeviceEntity>>> GetPageList(DeviceListParam param, Pagination pagination)
        {
            TData<List<DeviceEntity>> obj = new TData<List<DeviceEntity>>();
            obj.Result = await deviceService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<DeviceEntity>> GetEntity(long id)
        {
            TData<DeviceEntity> obj = new TData<DeviceEntity>();
            obj.Result = await deviceService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(DeviceEntity entity)
        {
            TData<string> obj = new TData<string>();
            await deviceService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await deviceService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
