﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using YiSha.Util;
using YiSha.Util.Extension;
using YiSha.Util.Model;
using YiSha.Entity.SystemManage;
using YiSha.Model.Param.SystemManage;
using YiSha.Service.SystemManage;

namespace YiSha.Business.SystemManage
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-09 20:09
    /// 描 述：业务类
    /// </summary>
    public class DaysBLL
    {
        private DaysService daysService = new DaysService();

        #region 获取数据
        public async Task<TData<List<DaysEntity>>> GetList(DaysListParam param)
        {
            TData<List<DaysEntity>> obj = new TData<List<DaysEntity>>();
            obj.Result = await daysService.GetList(param);
            obj.TotalCount = obj.Result.Count;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<List<DaysEntity>>> GetPageList(DaysListParam param, Pagination pagination)
        {
            TData<List<DaysEntity>> obj = new TData<List<DaysEntity>>();
            obj.Result = await daysService.GetPageList(param, pagination);
            obj.TotalCount = pagination.TotalCount;
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData<DaysEntity>> GetEntity(long id)
        {
            TData<DaysEntity> obj = new TData<DaysEntity>();
            obj.Result = await daysService.GetEntity(id);
            if (obj.Result != null)
            {
                obj.Tag = 1;
            }
            return obj;
        }
        #endregion

        #region 提交数据
        public async Task<TData<string>> SaveForm(DaysEntity entity)
        {
            TData<string> obj = new TData<string>();
            await daysService.SaveForm(entity);
            obj.Result = entity.Id.ParseToString();
            obj.Tag = 1;
            return obj;
        }

        public async Task<TData> DeleteForm(string ids)
        {
            TData obj = new TData();
            await daysService.DeleteForm(ids);
            obj.Tag = 1;
            return obj;
        }
        #endregion

        #region 私有方法
        #endregion
    }
}
