﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.DeviceManage;
using YiSha.Business.DeviceManage;
using YiSha.Model.Param.DeviceManage;

namespace YiSha.Admin.Web.Areas.DeviceManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-05-05 12:22
    /// 描 述：控制器类
    /// </summary>
    [Area("DeviceManage")]
    public class Module4Controller :  BaseController
    {
        private Module4BLL module4BLL = new Module4BLL();

        #region 视图功能
        [AuthorizeFilter("device:module4:view")]
        public ActionResult Module4Index()
        {
            return View();
        }

        public ActionResult Module4Form()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("device:module4:search")]
        public async Task<ActionResult> GetListJson(Module4ListParam param)
        {
            TData<List<Module4Entity>> obj = await module4BLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("device:module4:search")]
        public async Task<ActionResult> GetPageListJson(Module4ListParam param, Pagination pagination)
        {
            TData<List<Module4Entity>> obj = await module4BLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<Module4Entity> obj = await module4BLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("device:module4:add,device:module4:edit")]
        public async Task<ActionResult> SaveFormJson(Module4Entity entity)
        {
            TData<string> obj = await module4BLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("device:module4:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await module4BLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
