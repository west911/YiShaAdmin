﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.DeviceManage;
using YiSha.Business.DeviceManage;
using YiSha.Model.Param.DeviceManage;

namespace YiSha.Admin.Web.Areas.DeviceManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:23
    /// 描 述：控制器类
    /// </summary>
    [Area("DeviceManage")]
    public class DeviceController :  BaseController
    {
        private DeviceBLL deviceBLL = new DeviceBLL();

        #region 视图功能
        [AuthorizeFilter("device:device:view")]
        public ActionResult DeviceIndex()
        {
            return View();
        }

        public ActionResult DeviceForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("device:device:search")]
        public async Task<ActionResult> GetListJson(DeviceListParam param)
        {
            TData<List<DeviceEntity>> obj = await deviceBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("device:device:search")]
        public async Task<ActionResult> GetPageListJson(DeviceListParam param, Pagination pagination)
        {
            TData<List<DeviceEntity>> obj = await deviceBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<DeviceEntity> obj = await deviceBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("device:device:add,device:device:edit")]
        public async Task<ActionResult> SaveFormJson(DeviceEntity entity)
        {
            TData<string> obj = await deviceBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("device:device:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await deviceBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
