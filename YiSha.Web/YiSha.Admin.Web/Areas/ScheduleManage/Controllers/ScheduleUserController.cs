﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ScheduleManage;
using YiSha.Business.ScheduleManage;
using YiSha.Model.Param.ScheduleManage;
using System.Text;
using YiSha.Enum;

namespace YiSha.Admin.Web.Areas.ScheduleManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 22:59
    /// 描 述：控制器类
    /// </summary>
    [Area("ScheduleManage")]
    public class ScheduleUserController :  BaseController
    {
        private ScheduleUserBLL scheduleUserBLL = new ScheduleUserBLL();

        #region 视图功能
        [AuthorizeFilter("schedule:scheduleuser:view")]
        public ActionResult ScheduleUserIndex()
        {
            return View();
        }

        public ActionResult ScheduleUserForm()
        {
            return View();
        }

        public ActionResult ScheduleUserCopyForm()
        {
            return View();
        }

        public ActionResult ScheduleUserDelForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("schedule:scheduleuser:search")]
        public async Task<ActionResult> GetListJson(ScheduleUserListParam param)
        {
            TData<List<ScheduleUserMonthEntity>> obj = await scheduleUserBLL.GetListByMonth(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("schedule:scheduledevice:search")]
        public async Task<ActionResult> GetList(ScheduleUserListParam param)
        {
            TData<List<ScheduleUserEntity>> obj = await scheduleUserBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("schedule:scheduleuser:search")]
        public async Task<ActionResult> GetPageListJson(ScheduleUserListParam param, Pagination pagination)
        {
            TData<List<ScheduleUserEntity>> obj = await scheduleUserBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ScheduleUserEntity> obj = await scheduleUserBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]

        public async Task<ActionResult> CopyFormJson(ScheduleUserListParam param)
        {
            ScheduleUserListParam entity = new ScheduleUserListParam();
            entity.Date = param.Date;
            entity.EndDate = param.Date;
            TData<List<ScheduleUserEntity>> obj = await scheduleUserBLL.GetList(entity);
            TData<string> rtObj = new TData<string>();
            if (obj.Result.Count > 0)
            {
                foreach (ScheduleUserEntity deviceEntity in obj.Result)
                {
                    deviceEntity.Id = null;
                    deviceEntity.Year = param.EndDate.Value.Year;
                    deviceEntity.Month = param.EndDate.Value.Month;
                    deviceEntity.Day = param.EndDate.Value.Day;
                    deviceEntity.Date = (DateTime)param.EndDate;
                    deviceEntity.CDay = System.Enum.GetName(typeof(DaysEnum), param.EndDate.Value.Day);
                    rtObj = await scheduleUserBLL.SaveForm(deviceEntity);
                }
            }
            else
            {
                rtObj.Message = "没有可复制的日程";
            }
            return Json(rtObj);
        }

        [HttpPost]

        public async Task<ActionResult> DelFormJson(ScheduleUserListParam param)
        {
            ScheduleUserListParam entity = new ScheduleUserListParam();
            entity.Date = param.Date;
            entity.EndDate = param.Date;
            TData<List<ScheduleUserEntity>> obj = await scheduleUserBLL.GetList(entity);
            StringBuilder str = new StringBuilder();
            foreach (ScheduleUserEntity deviceEntity in obj.Result)
            {
                str.Append(deviceEntity.Id);
                str.Append(",");
            }
            TData rtObj = await scheduleUserBLL.DeleteForm(str.ToString());
            return Json(rtObj);
        }

        [HttpPost]
        [AuthorizeFilter("schedule:scheduleuser:add,schedule:scheduleuser:edit")]
        public async Task<ActionResult> SaveFormJson(SaveScheduleUserEntity entity)
        {
            string[] userIds = entity.UserId.Split(',');
            TData<string> obj = new TData<string>();
            for (DateTime sDate = entity.StartDate; sDate <= entity.EndDate; sDate = sDate.AddDays(1))
            {
                foreach (string userId in userIds)
                {
                    ScheduleUserEntity userEntity = new ScheduleUserEntity()
                    {
                        UserId = userId,
                        ProductId = entity.ProductId,
                        Date = sDate.Date,
                        Year = sDate.Year,
                        Month = sDate.Month,
                        Day = sDate.Day,
                        CDay = System.Enum.GetName(typeof(DaysEnum), sDate.Day),
                        Count = entity.Count,
                        AccountId = entity.AccountId,
                        AccountName = entity.AccountName,
                        AccountAddr = entity.AccountAddr
                    };
                    obj = await scheduleUserBLL.SaveForm(userEntity);
                }
            }
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("schedule:scheduleuser:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await scheduleUserBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
