﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ScheduleManage;
using YiSha.Business.ScheduleManage;
using YiSha.Model.Param.ScheduleManage;
using System.Text;
using YiSha.Enum;

namespace YiSha.Admin.Web.Areas.ScheduleManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-03 17:08
    /// 描 述：控制器类
    /// </summary>
    [Area("ScheduleManage")]
    public class ScheduleController :  BaseController
    {
        private ScheduleDeviceBLL scheduleDeviceBLL = new ScheduleDeviceBLL();
        private ScheduleUserBLL scheduleUserBLL = new ScheduleUserBLL();

        #region 视图功能
        [AuthorizeFilter("schedule:schedule:view")]
        public ActionResult ScheduleIndex()
        {
            return View();
        }

        public ActionResult ScheduleForm()
        {
            return View();
        }

        public ActionResult ScheduleCopyForm()
        {
            return View();
        }

        public ActionResult ScheduleDelForm()
        {
            return View();
        }

        public ActionResult ScheduleDetail(DateTime date, string userId, string deviceNo)
        {
            ViewBag.date = date.ToShortDateString();
            ViewBag.userid = userId;
            ViewBag.deviceno = deviceNo;
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("schedule:schedule:search")]
        public async Task<ActionResult> GetListJson(ScheduleDeviceListParam param)
        {
            TData<List<ScheduleDeviceMonthEntity>> obj = await scheduleDeviceBLL.GetListByMonth(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("schedule:schedule:search")]
        public async Task<ActionResult> GetAllListJson(ScheduleListParam param)
        {
            TData<List<ScheduleDeviceMonthEntity>> objDevice = await scheduleDeviceBLL.GetAllListByDate(param);
            TData<List<ScheduleUserMonthEntity>> objUser = await scheduleUserBLL.GetAllListByDate(param);
            TData<List<ScheduleEntity>> obj = new TData<List<ScheduleEntity>>();
            obj.Result = new List<ScheduleEntity>();
            obj.Tag = objDevice.Tag;
            foreach (var entity in objUser.Result)
            {
                obj.Result.Add(new ScheduleEntity
                {
                    Name = entity.UserName,
                    No = entity.UserId.ToString(),
                    Count = 1,
                    一 = entity.一,
                    二 = entity.二,
                    三 = entity.三,
                    四 = entity.四,
                    五 = entity.五,
                    六 = entity.六,
                    七 = entity.七,
                    八 = entity.八,
                    九 = entity.九,
                    十 = entity.十,
                    十一 = entity.十一,
                    十二 = entity.十二,
                    十三 = entity.十三,
                    十四 = entity.十四,
                    十五 = entity.十五,
                    十六 = entity.十六,
                    十七 = entity.十七,
                    十八 = entity.十八,
                    十九 = entity.十九,
                    二十 = entity.二十,
                    二十一 = entity.二十一,
                    二十二 = entity.二十二,
                    二十三 = entity.二十三,
                    二十四 = entity.二十四,
                    二十五 = entity.二十五,
                    二十六 = entity.二十六,
                    二十七 = entity.二十七,
                    二十八 = entity.二十八,
                    二十九 = entity.二十九,
                    三十 = entity.三十,
                    三十一 = entity.三十一
                });
                obj.TotalCount++;
            }
            foreach (var entity in objDevice.Result)
            {
                obj.Result.Add(new ScheduleEntity
                {
                    Name = entity.DeviceName + ":"+ entity.DeviceNo,
                    No = entity.DeviceNo,
                    Count = entity.DeviceCount,
                    一 = entity.一,
                    二 = entity.二,
                    三 = entity.三,
                    四 = entity.四,
                    五 = entity.五,
                    六 = entity.六,
                    七 = entity.七,
                    八 = entity.八,
                    九 = entity.九,
                    十 = entity.十,
                    十一 = entity.十一,
                    十二 = entity.十二,
                    十三 = entity.十三,
                    十四 = entity.十四,
                    十五 = entity.十五,
                    十六 = entity.十六,
                    十七 = entity.十七,
                    十八 = entity.十八,
                    十九 = entity.十九,
                    二十 = entity.二十,
                    二十一 = entity.二十一,
                    二十二 = entity.二十二,
                    二十三 = entity.二十三,
                    二十四 = entity.二十四,
                    二十五 = entity.二十五,
                    二十六 = entity.二十六,
                    二十七 = entity.二十七,
                    二十八 = entity.二十八,
                    二十九 = entity.二十九,
                    三十 = entity.三十,
                    三十一 = entity.三十一
                });
                obj.TotalCount++;
            }
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("schedule:schedule:search")]
        public async Task<ActionResult> GetList(ScheduleDeviceListParam param)
        {
            TData<List<ScheduleDeviceEntity>> obj = await scheduleDeviceBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("schedule:schedule:search")]
        public async Task<ActionResult> GetPageListJson(ScheduleDeviceListParam param, Pagination pagination)
        {
            TData<List<ScheduleDeviceEntity>> obj = await scheduleDeviceBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ScheduleDeviceEntity> obj = await scheduleDeviceBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        
        public async Task<ActionResult> CopyFormJson(ScheduleDeviceListParam param)
        {
            ScheduleDeviceListParam entity = new ScheduleDeviceListParam();
            entity.Date = param.Date;
            entity.EndDate = param.Date;
            TData<List<ScheduleDeviceEntity>> obj = await scheduleDeviceBLL.GetList(entity);
            TData<string> rtObj = new TData<string>();
            if (obj.Result.Count > 0)
            {
                foreach (ScheduleDeviceEntity deviceEntity in obj.Result)
                {
                    deviceEntity.Id = null;
                    deviceEntity.Year = param.EndDate.Value.Year;
                    deviceEntity.Month = param.EndDate.Value.Month;
                    deviceEntity.Day = param.EndDate.Value.Day;
                    deviceEntity.Date = (DateTime)param.EndDate;
                    deviceEntity.CDay = System.Enum.GetName(typeof(DaysEnum), param.EndDate.Value.Day);
                    rtObj = await scheduleDeviceBLL.SaveForm(deviceEntity);
                }
            }
            else 
            {
                rtObj.Message = "没有可复制的日程";
            }
            return Json(rtObj);
        }

        [HttpPost]

        public async Task<ActionResult> DelFormJson(ScheduleDeviceListParam param)
        {
            ScheduleDeviceListParam entity = new ScheduleDeviceListParam();
            entity.Date = param.Date;
            entity.EndDate = param.Date;
            TData<List<ScheduleDeviceEntity>> obj = await scheduleDeviceBLL.GetList(entity);
            StringBuilder str = new StringBuilder();
            foreach (ScheduleDeviceEntity deviceEntity in obj.Result)
            {
                str.Append(deviceEntity.Id);
                str.Append(",");
            }
            TData rtObj = await scheduleDeviceBLL.DeleteForm(str.ToString());
            return Json(rtObj);
        }

        [HttpPost]
        [AuthorizeFilter("schedule:schedule:add,schedule:schedule:edit")]
        public async Task<ActionResult> SaveFormJson(SaveScheduleDeviceEntity entity)
        {
            string[] deviceNames = entity.DeviceName.Split(',');
            TData<string> obj = new TData<string>();
            for (DateTime sDate = entity.StartDate; sDate <= entity.EndDate; sDate = sDate.AddDays(1))
            {
                foreach (string deviceName in deviceNames)
                {
                    string[] device = deviceName.Split(':');
                    ScheduleDeviceEntity deviceEntity = new ScheduleDeviceEntity()
                    {
                        DeviceName = device.Length > 0 ? device[0] : deviceName,
                        DeviceNo = device.Length > 1 ? device[1] : "",
                        ProductId = entity.ProductId,
                        Date = sDate.Date,
                        Year = sDate.Year,
                        Month = sDate.Month,
                        Day = sDate.Day,
                        CDay = System.Enum.GetName(typeof(DaysEnum), sDate.Day),
                        Count = entity.Count,
                        AccountId = entity.AccountId,
                        AccountName = entity.AccountName,
                        AccountAddr = entity.AccountAddr
                    };
                    obj = await scheduleDeviceBLL.SaveForm(deviceEntity);
                }
            }

            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("schedule:schedule:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await scheduleDeviceBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
