﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ProductManage;
using YiSha.Business.ProductManage;
using YiSha.Model.Param.ProductManage;

namespace YiSha.Admin.Web.Areas.ProductManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:05
    /// 描 述：控制器类
    /// </summary>
    [Area("ProductManage")]
    public class ProductUserController :  BaseController
    {
        private ProductUserBLL productUserBLL = new ProductUserBLL();

        #region 视图功能
        [AuthorizeFilter("product:productuser:view")]
        public ActionResult ProductUserIndex()
        {
            return View();
        }

        public ActionResult ProductUserForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("product:productuser:search")]
        public async Task<ActionResult> GetListJson(ProductUserListParam param)
        {
            TData<List<ProductUserEntity>> obj = await productUserBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("product:productuser:search")]
        public async Task<ActionResult> GetPageListJson(ProductUserListParam param, Pagination pagination)
        {
            TData<List<ProductUserEntity>> obj = await productUserBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ProductUserEntity> obj = await productUserBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("product:productuser:add,product:productuser:edit")]
        public async Task<ActionResult> SaveFormJson(ProductUserEntity entity)
        {
            TData<string> obj = new TData<string>();
            TData<ProductUserEntity> oEntity = await productUserBLL.GetEntity((long)entity.Id);
            if (oEntity.Result == null || oEntity.Result.ProductId != entity.ProductId || oEntity.Result.UserId != entity.UserId)
            {
                TData<ProductUserEntity> sEntity = await productUserBLL.SearchEntity((long)entity.ProductId, (long)entity.UserId);
                if (sEntity.Result != null)
                {
                    obj.Tag = 0;
                    obj.Message = "关联已存在";
                    return Json(obj);
                }
            }
            obj = await productUserBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("product:productuser:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await productUserBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
