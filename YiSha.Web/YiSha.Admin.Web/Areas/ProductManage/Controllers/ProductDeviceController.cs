﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ProductManage;
using YiSha.Business.ProductManage;
using YiSha.Model.Param.ProductManage;

namespace YiSha.Admin.Web.Areas.ProductManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-04 23:07
    /// 描 述：控制器类
    /// </summary>
    [Area("ProductManage")]
    public class ProductDeviceController :  BaseController
    {
        private ProductDeviceBLL productDeviceBLL = new ProductDeviceBLL();

        #region 视图功能
        [AuthorizeFilter("product:productdevice:view")]
        public ActionResult ProductDeviceIndex()
        {
            return View();
        }

        public ActionResult ProductDeviceForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("product:productdevice:search")]
        public async Task<ActionResult> GetListJson(ProductDeviceListParam param)
        {
            TData<List<ProductDeviceEntity>> obj = await productDeviceBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("product:productdevice:search")]
        public async Task<ActionResult> GetNOListJson(ProductDeviceListParam param)
        {
            TData<List<ProductDeviceEntity>> obj = await productDeviceBLL.GetNOList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("product:productdevice:search")]
        public async Task<ActionResult> GetPageListJson(ProductDeviceListParam param, Pagination pagination)
        {
            TData<List<ProductDeviceEntity>> obj = await productDeviceBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ProductDeviceEntity> obj = await productDeviceBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("product:productdevice:add,product:productdevice:edit")]
        public async Task<ActionResult> SaveFormJson(ProductDeviceEntity entity)
        {
            TData<string> obj = new TData<string>();
            TData<ProductDeviceEntity> oEntity = await productDeviceBLL.GetEntity((long)entity.Id);
            if (oEntity.Result == null || oEntity.Result.ProductId != entity.ProductId || oEntity.Result.DeviceName != entity.DeviceName)
            {
                TData<ProductDeviceEntity> sEntity = await productDeviceBLL.SearchEntity((long)entity.ProductId, entity.DeviceName);
                if (sEntity.Result != null)
                {
                    obj.Tag = 0;
                    obj.Message = "关联已存在";
                    return Json(obj);
                }
            }
            obj = await productDeviceBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("product:productdevice:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await productDeviceBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
