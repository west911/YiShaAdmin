﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.SystemManage;
using YiSha.Business.SystemManage;
using YiSha.Model.Param.SystemManage;

namespace YiSha.Admin.Web.Areas.SystemManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-09 20:09
    /// 描 述：控制器类
    /// </summary>
    [Area("SystemManage")]
    public class DaysController :  BaseController
    {
        private DaysBLL daysBLL = new DaysBLL();

        #region 视图功能
        [AuthorizeFilter("system:days:view")]
        public ActionResult DaysIndex()
        {
            return View();
        }

        public ActionResult DaysForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("system:days:search")]
        public async Task<ActionResult> GetListJson(DaysListParam param)
        {
            TData<List<DaysEntity>> obj = await daysBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("system:days:search")]
        public async Task<ActionResult> GetPageListJson(DaysListParam param, Pagination pagination)
        {
            TData<List<DaysEntity>> obj = await daysBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<DaysEntity> obj = await daysBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("system:days:add,system:days:edit")]
        public async Task<ActionResult> SaveFormJson(DaysEntity entity)
        {
            TData<string> obj = await daysBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("system:days:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await daysBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
