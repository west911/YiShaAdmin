﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ExperimentManage;
using YiSha.Business.ExperimentManage;
using YiSha.Model.Param.ExperimentManage;
using YiSha.Web.Code;
using Newtonsoft.Json;

namespace YiSha.Admin.Web.Areas.ExperimentManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:30
    /// 描 述：控制器类
    /// </summary>
    [Area("ExperimentManage")]
    public class ExperimentDataController :  BaseController
    {
        private ExperimentDataBLL experimentDataBLL = new ExperimentDataBLL();

        #region 视图功能
        [AuthorizeFilter("experiment:experimentdata:view")]
        public async Task<ActionResult> ExperimentDataIndex()
        {
            OperatorInfo user = await Operator.Instance.Current();
            ViewBag.User = JsonConvert.SerializeObject(user);
            return View();
        }

        public async Task<ActionResult> ExperimentDataForm()
        {
            OperatorInfo user = await Operator.Instance.Current();
            ViewBag.User = JsonConvert.SerializeObject(user);
            return View();
        }

        public async Task<ActionResult> ExperimentDataPreview()
        {
            OperatorInfo user = await Operator.Instance.Current();
            return View();
        }

        public async Task<ActionResult> ExperimentCleanPreview()
        {
            OperatorInfo user = await Operator.Instance.Current();
            return View();
        }

        public ActionResult SelectExpTypeForm()
        {
            return View();
        }

        public async Task<ActionResult> ExperimentCleanForm()
        {
            OperatorInfo user = await Operator.Instance.Current();
            ViewBag.User = JsonConvert.SerializeObject(user);
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("experiment:experimentdata:search")]
        public async Task<ActionResult> GetListJson(ExperimentDataListParam param)
        {
            TData<List<ExperimentDataEntity>> obj = await experimentDataBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("experiment:experimentdata:search")]
        public async Task<ActionResult> GetPageListJson(ExperimentDataListParam param, Pagination pagination)
        {
            TData<List<ExperimentDataEntity>> obj = await experimentDataBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("experiment:experimentdata:search")]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ExperimentDataEntity> obj = await experimentDataBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("experiment:experimentdata:add,experiment:experimentdata:edit")]
        public async Task<ActionResult> SaveFormJson(ExperimentDataEntity entity)
        {
            OperatorInfo user = await Operator.Instance.Current();
            switch (entity.Status)
            {
                case 1:
                    entity.OwnerIds = user.UserId;
                    entity.SumbitDatetime = DateTime.Now;
                    entity.ReviewIds = 0;
                    entity.ConfirmIds = 0;
                    entity.ReviewDatetime = null;
                    entity.ConfirmDatetime = null;
                    break;
                case 2:
                case 3:
                    entity.ReviewIds = user.UserId;
                    entity.ReviewDatetime = DateTime.Now;
                    break;
                case 4:
                case 5:
                    entity.ConfirmIds = user.UserId;
                    entity.ConfirmDatetime = DateTime.Now;
                    break;
                default:
                    entity.OwnerIds = user.UserId;
                    entity.SumbitDatetime = null;
                    break;
            }
            TData<string> obj = await experimentDataBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("experiment:experimentdata:add,experiment:experimentdata:edit")]
        public async Task<ActionResult> ChangeStatus(long id, int status)
        {
            OperatorInfo user = await Operator.Instance.Current();
            TData<ExperimentDataEntity> obj = await experimentDataBLL.GetEntity(id);
            obj.Result.Status = status;
            switch (status)
            {
                case 1:
                    obj.Result.OwnerIds = user.UserId;
                    obj.Result.SumbitDatetime = DateTime.Now;
                    obj.Result.ReviewIds = 0;
                    obj.Result.ConfirmIds = 0;
                    obj.Result.ReviewDatetime = null;
                    obj.Result.ConfirmDatetime = null;
                    break;
                case 2:
                case 3:
                    obj.Result.ReviewIds = user.UserId;
                    obj.Result.ReviewDatetime = DateTime.Now;
                    break;
                case 4:
                case 5:
                    obj.Result.ConfirmIds = user.UserId;
                    obj.Result.ConfirmDatetime = DateTime.Now;
                    break;
                default:
                    obj.Result.OwnerIds = user.UserId;
                    obj.Result.SumbitDatetime = null;
                    break;
            }
            TData<string> obj2 = await experimentDataBLL.SaveForm(obj.Result);
            return Json(obj2);
        }

        [HttpPost]
        [AuthorizeFilter("experiment:experimentdata:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await experimentDataBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
