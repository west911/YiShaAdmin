﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.ExperimentManage;
using YiSha.Business.ExperimentManage;
using YiSha.Model.Param.ExperimentManage;
using YiSha.Web.Code;

namespace YiSha.Admin.Web.Areas.ExperimentManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-21 03:33
    /// 描 述：控制器类
    /// </summary>
    [Area("ExperimentManage")]
    public class ExperimentFlowController :  BaseController
    {
        private ExperimentFlowBLL experimentFlowBLL = new ExperimentFlowBLL();

        #region 视图功能
        [AuthorizeFilter("experiment:experimentflow:view")]
        public ActionResult ExperimentFlowIndex()
        {
            return View();
        }

        public ActionResult ExperimentFlowForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("experiment:experimentflow:search")]
        public async Task<ActionResult> GetListJson(ExperimentFlowListParam param)
        {
            TData<List<ExperimentFlowEntity>> obj = await experimentFlowBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("experiment:experimentflow:search")]
        public async Task<ActionResult> GetPageListJson(ExperimentFlowListParam param, Pagination pagination)
        {
            TData<List<ExperimentFlowEntity>> obj = await experimentFlowBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<ExperimentFlowEntity> obj = await experimentFlowBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("experiment:experimentflow:add,experiment:experimentflow:edit")]
        public async Task<ActionResult> SaveFormJson(ExperimentFlowEntity entity)
        {
            OperatorInfo user = await Operator.Instance.Current();
            entity.OwnerIds = user.UserId.ToString();
            TData<string> obj = await experimentFlowBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("experiment:experimentflow:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await experimentFlowBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
