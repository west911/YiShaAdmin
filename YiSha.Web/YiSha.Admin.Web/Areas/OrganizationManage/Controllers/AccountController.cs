﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
using YiSha.Util;
using YiSha.Util.Model;
using YiSha.Entity;
using YiSha.Model;
using YiSha.Admin.Web.Controllers;
using YiSha.Entity.OrganizationManage;
using YiSha.Business.OrganizationManage;
using YiSha.Model.Param.OrganizationManage;

namespace YiSha.Admin.Web.Areas.OrganizationManage.Controllers
{
    /// <summary>
    /// 创 建：admin
    /// 日 期：2020-04-13 08:57
    /// 描 述：控制器类
    /// </summary>
    [Area("OrganizationManage")]
    public class AccountController :  BaseController
    {
        private AccountBLL accountBLL = new AccountBLL();

        #region 视图功能
        [AuthorizeFilter("organization:account:view")]
        public ActionResult AccountIndex()
        {
            return View();
        }

        public ActionResult AccountForm()
        {
            return View();
        }
        #endregion

        #region 获取数据
        [HttpGet]
        [AuthorizeFilter("organization:account:search")]
        public async Task<ActionResult> GetListJson(AccountListParam param)
        {
            TData<List<AccountEntity>> obj = await accountBLL.GetList(param);
            return Json(obj);
        }

        [HttpGet]
        [AuthorizeFilter("organization:account:search")]
        public async Task<ActionResult> GetPageListJson(AccountListParam param, Pagination pagination)
        {
            TData<List<AccountEntity>> obj = await accountBLL.GetPageList(param, pagination);
            return Json(obj);
        }

        [HttpGet]
        public async Task<ActionResult> GetFormJson(long id)
        {
            TData<AccountEntity> obj = await accountBLL.GetEntity(id);
            return Json(obj);
        }
        #endregion

        #region 提交数据
        [HttpPost]
        [AuthorizeFilter("organization:account:add,organization:account:edit")]
        public async Task<ActionResult> SaveFormJson(AccountEntity entity)
        {
            TData<string> obj = await accountBLL.SaveForm(entity);
            return Json(obj);
        }

        [HttpPost]
        [AuthorizeFilter("organization:account:delete")]
        public async Task<ActionResult> DeleteFormJson(string ids)
        {
            TData obj = await accountBLL.DeleteForm(ids);
            return Json(obj);
        }
        #endregion
    }
}
